<?php namespace App;

use Illuminate\Database\Eloquent\Model;


class Notas extends Model {

	protected $table = 'notas';
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['id_usuario','fecha', 'importante', 'nota'];

}
