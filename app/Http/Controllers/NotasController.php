<?php namespace App\Http\Controllers;
use Illuminate\Routing\Redirector;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Request;
use Illuminate\Contracts\Auth\Guard;
use App\Notas;



class NotasController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
		$this->middleware('auth');
	}
	/**
	 * Show the application Notas screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		  $user = $this->auth->user()->id;
		  $notast = \DB::table('notas')
		->select(['fecha', 'importante','nota'])
		->where('id_usuario', $user)
		->paginate();
		return view('notas', compact('notast'));
	}
	public function store()
	{
		$nota = new Notas(Request::all());
		$nota->save();
		return redirect()->route('admin.notas.index');
	}
	

}
