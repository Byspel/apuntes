/*
SQLyog Ultimate v11.3 (64 bit)
MySQL - 5.6.21 : Database - dblaravel
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`dblaravel` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `dblaravel`;

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`migration`,`batch`) values ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1);

/*Table structure for table `notas` */

DROP TABLE IF EXISTS `notas`;

CREATE TABLE `notas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_usuario` int(10) unsigned NOT NULL,
  `fecha` date DEFAULT NULL,
  `importante` char(1) DEFAULT NULL,
  `nota` text,
  `estado` char(1) DEFAULT '1' COMMENT 'P=Poco Importante, N=Normal, I=Muy importante',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `id_usuario` (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `notas` */

insert  into `notas`(`id`,`id_usuario`,`fecha`,`importante`,`nota`,`estado`,`updated_at`,`created_at`) values (1,1,'2015-05-12','I','MODULO DE LABORATORIO','1','2015-05-12 21:16:27','2015-05-12 21:16:27'),(2,1,'2015-05-14','N','xxdd','1','2015-05-12 21:27:39','2015-05-12 21:27:39'),(3,2,'2015-05-13','P','MODULO DE LABORATORIO','1','2015-05-12 21:36:44','2015-05-12 21:36:44'),(4,2,'2015-05-12','I','MODULO DE CITAS','1','2015-05-12 21:39:53','2015-05-12 21:39:53'),(6,2,'2015-05-12','N','dxd','1','2015-05-12 21:47:14','2015-05-12 21:47:14'),(7,1,'2015-05-14','N','Buscar el éxito ','1','2015-05-15 00:46:27','2015-05-15 00:46:27'),(8,1,'2015-05-14','N','Ser feliz ','1','2015-05-15 00:47:05','2015-05-15 00:47:05');

/*Table structure for table `notas2` */

DROP TABLE IF EXISTS `notas2`;

CREATE TABLE `notas2` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_usuario` int(10) unsigned NOT NULL,
  `fecha` date DEFAULT NULL,
  `importante` char(1) DEFAULT NULL,
  `nota` text,
  `estado` char(1) DEFAULT NULL COMMENT 'P=Poco Importante, N=Normal, I=Muy importante',
  `fecha_ingreso` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `id_usuario` (`id_usuario`),
  CONSTRAINT `notas2_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

/*Data for the table `notas2` */

insert  into `notas2`(`id`,`id_usuario`,`fecha`,`importante`,`nota`,`estado`,`fecha_ingreso`) values (1,1,'2015-04-18','1','Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.','1','2015-04-18 17:20:00'),(2,1,'2015-04-17','0','contanido de nota 2','1','2015-04-18 17:52:14'),(3,1,'2015-04-24','1','contenido de nota 3','1','2015-04-18 17:52:56'),(4,1,'2015-04-19','0','contenido de nota 4','1','2015-04-18 17:53:09'),(5,1,'2015-04-30','1','contenido de nota 5','1','2015-04-18 17:53:14'),(6,1,'2015-04-21','0','contenido de nota 6','1','2015-04-18 17:54:29'),(9,1,'2015-04-30','1','contenido de nota 7','1','2015-04-18 17:54:52'),(10,1,'2015-04-23','0','contenido de nota 8','1','2015-04-18 17:55:10'),(11,1,'2015-04-08','1','contenido de nota 9','1','2015-04-18 18:02:39'),(12,1,'2015-05-08','0','contenido de nota 10','1','2015-04-18 18:03:11'),(13,1,'2015-07-03','1','contenido de nota 11','1','2015-04-18 18:03:39'),(14,1,'2015-04-30','0','contenido de nota 12','1','2015-04-18 18:03:54'),(15,1,'2015-05-06','1','contenido de nota 13','1','2015-04-18 18:04:09'),(16,1,'2015-04-30','0','contenido de nota 14','1','2015-04-18 18:04:29'),(17,1,'2015-04-23','1','contenido de nota 15','1','2015-04-18 18:04:53'),(18,1,'2015-04-08','0','contenido de nota 16','1','2015-04-18 18:04:58');

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`password`,`remember_token`,`created_at`,`updated_at`) values (1,'Ivan','ivan@ivan.com','$2y$10$WYSJx1MuGG4xKfrB0V/6wuh8J9cf2bKfYW9/9DPjv/uhsMHcBr7HC','cyEbHaSsJfV2TMLFSptAalFk0tlMn7fD0fsjBrCzhdCWL7KFhD2YkYVqZ0aC','0000-00-00 00:00:00','2015-05-12 21:19:14'),(2,'byspel','idlh@byspel.com','$2y$10$BjIPMyH9uaGQeWYE9K7TYenSPCHQMX1.0kOsTekz3jCbwEn36Fi/e','BojONAw7tnc7FdbzMsfOmXK8f1oP8v4J3PRnOAIH4MxHFsD3lx2tqL8kWWAl','2015-04-17 18:48:05','2015-05-12 21:12:48');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
