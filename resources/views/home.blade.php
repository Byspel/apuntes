@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Inicio</div>

				<div class="panel-body">
					Bienvenido, Apuntes es una aplicación gratis puedes usarla cuando quieras durante el tiempo que quieras. :)
					
					<div class="row">
						<br>
						<div class="col-sm-6 col-md-4">
							<div class="thumbnail">

								<div class="caption">
									<h3>Nuevo Apunte</h3>
									<p>Añade todos cuantos quieras, no tienen limites. sabemos que manejas muchas cosas al tiempo así que animate a organizarte</p>
									<p><a href="{{route('admin.nueva.index')}}" class="btn btn-primary" role="button">Agregar</a></p>
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-md-4">
							<div class="thumbnail">
								
								<div class="caption">
									<h3>Mis Apuntes</h3>
									<p>Desde aquí obtendrás una vista rápida para la gestión de tus registros actuales, verás que fácil es indicar que terminaste algo.</p>
									<p><a href="{{route('admin.notas.index')}}" class="btn btn-primary" role="button">Verificar</a></p>
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-md-4">
							<div class="thumbnail">
								<div class="caption">
									<h3>Históricos</h3>
									<p>Tu información mas remota a tu alcance de tu mano manera rápida, establece un intervalo de fechas o verifica tu rendimiento	.</p>
									<p><a href="{{route('admin.history.index')}}" class="btn btn-primary" role="button">Verificar</a></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
