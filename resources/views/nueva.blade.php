@extends('app')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel-body">
				{!! Form::open(['route' => ['admin.notas.store', 'id_usuario' => Auth::user()->id], 'method'=> 'POST']) !!}
				
				<div class="form-group">
					<label class="col-md-4 control-label">Nivel</label>
					{!! Form::select('importante', ['I' => 'Importante', 'N' => 'Normal', 'P' => 'Poco Importante'], null ,['class' => 'form-control']) !!}
				</div>


				<div class="form-group">
					<label class="col-md-4 control-label">Fecha</label>
					{!! Form::date('fecha', null, ['class' => 'form-control']) !!}
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label">Observación</label>
					{!! Form::textArea('nota', null, ['class' => 'form-control', 'placeholder' => 'Por favor ingrese su apunte', 'Request']) !!}
				</div>

				<div class="form-group">
					<div class="col-md-6 col-md-offset-4">
						<button type="submit" class="btn btn-primary">Crear Apunte</button>
					</div>	
				</div>

				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@endsection
