@extends('app')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<p><b>{{ $notast->total() }}</b> Apuntes</p>
			<a class="btn btn-primary" href="{{ route('admin.nueva.index') }}" role="form-control">Nuevo Apunte</a>
			<table class="table table-striped">
				<tr>
					<th>Fecha</th>
					<th>Imp</th>
					<th>Nota</th>
					<th>Acción</th>
				</tr>
				@foreach ($notast as $nota)
				<tr>
					<td>{{ $nota->fecha }}</td>
					<td>{{ $nota->importante }}</td>
					<td>{{ $nota->nota}}</td>
					<td>
						<div class="btn-group">
							<button class="btn btn-primary btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">Acción <span class="caret"></span>
							</button>
							<ul class="dropdown-menu" role="menu">
								<li><a href="#">Finalizar</a></li>
								<li><a onclick="shoW();">Eliminar</a></li>
							</ul>
						</div>
					</td>
				</tr>
				@endforeach
			</table>
			{!! $notast->render() !!}
		</div>
	</div>
	   <div class="modal fade" id="modalE" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title">Confirmación</h4>
            </div>
            <form role="form" action="" name="frmClientes" onsubmit="Registrar(idP,accion); return false">
              <div class="col-lg-12">
              	<br>
              	<p>¡Un momento! ¿En realidad desea Eliminar este apunte?</p>
                <button type="submit" class="btn btn-primary btn-xs">
                  <span class="glyphicon glyphicon-star" aria-hidden="true"></span> Eliminar
                </button>
              </div>
            </form>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-times"></i>x</button>
            </div>
          </div>
      </div>
  </div>
  <script type="text/javascript">
  function shoW () {
  	$('#modalE').modal('show');
  }
  </script>
</div>
@endsection
